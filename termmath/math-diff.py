#!/usr/bin/python3

# Brute total
# Vou somar todas as colunas. Caso uma linha tenha conteúdo não numérico, a soma
# dessa coluna será NaN.

# Melhoramentos
# Posso pedir um display humano para os inteiros

from sys import stdin, stdout

old_v = []
old_n = 0
old_t = []
while True:
	line = stdin.readline()
	if line == "":
		break
	
	# Estou a assumir que o python vai separar os vários campos por conjuntos de
	# espaços e tabs.
	fields = line.strip().split()
	new_n = len(fields)
	new_v = [0]*new_n
	new_t = [0]*new_n

	# Teste dos campos de input
	# Codigos dos types: 0:int, 1:float, 2:NaN
	for i in range(new_n):

		# Isto basicamente testa se o field é um número
		try:
			x = float(fields[i])
		except ValueError:
			new_t[i] = 2
			continue

		# Aqui afina a classificação do número, entre inteiro ou float
		try:
			x_int = int(fields[i])
			new_v[i] = x_int
			continue
		except ValueError:
			new_t[i] = 1
		
		new_v[i] = x
	
	#print(new_t)
	
	run_n = min(old_n, new_n)
	
	if run_n == 0:
		old_v = new_v
		old_n = new_n
		old_t = new_t
		continue
	
	stdout.write("%g"%(new_v[0] - old_v[0]))
	for i in range(1, run_n):
		if new_t[i] == 2 or old_t[i] == 2:
			stdout.write('\t')
			continue
		
		# Print com formatação inteira, caso ambos sejam inteiros.
		if new_t[i]==0 and old_t[i]==0:
			stdout.write("\t%d"%(new_v[i] - old_v[i]))
		else:
			stdout.write("\t%g"%(new_v[i] - old_v[i]))
	
	stdout.write("\n")
	
	# Actualização do old vector
	old_v = new_v
	old_n = new_n
	old_t = new_t

exit(0)
