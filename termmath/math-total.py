#!/usr/bin/python3

# Brute total
# Vou somar todas as colunas. Caso uma linha tenha conteúdo não numérico, a soma
# dessa coluna será NaN.

# Melhoramentos
# Posso pedir um display humano para os inteiros

from sys import stdin, stdout

total  = []
is_int = []
valid  = []
N = 0
while True:
	line = stdin.readline()
	if line == "":
		break
	
	# Estou a assumir que o python vai separar os vários campos por conjuntos de
	# espaços e tabs.
	fields = line.split()
	nf = len(fields)

	lim_max = min(N, nf)
	
	# Somas para os campos existentes
	for i in range(lim_max):

		if not valid[i]:
			continue
		
		# Isto basicamente testa se o field é um número
		try:
			v = float(fields[i])
		except ValueError:
			valid[i] = False
			is_int[i] = False
			continue
		
		# Aqui afina a classificação do número, entre inteiro ou float
		if is_int[i]:
			try:
				v_int = int(fields[i])
				total[i] += v_int
				continue
			except ValueError:
				is_int[i] = False

		total[i] += v
	
	# Somas novas
	n_rest = nf - lim_max
	total_new  = [0]    * n_rest
	is_int_new = [True] * n_rest
	valid_new  = [True] * n_rest
	for i in range(n_rest):

		try:
			v = float(fields[lim_max + i])
		except ValueError:
			valid_new[i] = False
			is_int_new[i] = False
			continue

		try:
			v_int = int(fields[lim_max + i])
			total_new[i] = v_int
			continue
		except ValueError:
			is_int_new[i] = False
		
		total_new[i] = v
	
	total  += total_new
	is_int += is_int_new
	valid  += valid_new

	if nf > N:
		N = nf

if N == 0:
	exit(2)

# Output de todos os campos excepto o último
for i in range(N-1):
	if not valid[i]:
		stdout.write("NaN\t")
	elif is_int[i]:
		stdout.write("%d\t"%total[i])
	else:
		stdout.write("%g\t"%total[i])

# Output do último campo
if not valid[N-1]:
	stdout.write("NaN\n")
elif is_int[N-1]:
	stdout.write("%d\n"%total[N-1])
else:
	stdout.write("%g\n"%total[N-1])
		
exit(0)
