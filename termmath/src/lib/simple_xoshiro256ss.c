#include <stdint.h>

// Simplified version of xoshiro256starstar

// WARNING: This random number generator is _not_ cryptographically safe.

static inline uint64_t rotl(const uint64_t x, int k) {
	return (x << k) | (x >> (64 - k));
}


static uint64_t s[4];


// Custom function - not part of original algorithm
void xoshiro256ss_urandomSeed() {
	FILE* fp = fopen("/dev/urandom", "rb");
	uint8_t i,j;
	for(i=0; i<4; i++){
		s[i] = 0;
		for(j=0; j<8; j++){
			s[i] |= (uint64_t)fgetc(fp)<<(8*j);
		}
	}
	fclose(fp);
}


uint64_t xoshiro256ss_next(void) {
	const uint64_t result_starstar = rotl(s[1] * 5, 7) * 9;

	const uint64_t t = s[1] << 17;

	s[2] ^= s[0];
	s[3] ^= s[1];
	s[1] ^= s[2];
	s[0] ^= s[3];

	s[2] ^= t;

	s[3] = rotl(s[3], 45);

	return result_starstar;
}


// Custom function - not part of original algorithm
uint64_t xoshiro256ss_uniform(uint64_t n){
	const uint64_t u64_max = 0xffffffffffffffff;
	uint64_t lim_max = u64_max - u64_max%n;
	uint64_t r;
	do{
		r = xoshiro256ss_next();
	}while(r>=lim_max);
	return r%n;
}
