# pragma once

#include <stdlib.h>

typedef struct buff{
	int rows_len;
	int cols_len;
	double** rows;
	int* cols_active;
	int row_index;
} buff_t;

/*
	buff_t initialization to default values

	To be safe, it assumes no previous allocations.
	The allocations in this function must be ready for realloc
*/
int Buff_init(buff_t* b);


/*
	Reads stdin and populates a numerical matrix.
*/
buff Buff_read_stdin();

