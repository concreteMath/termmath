#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<inttypes.h>

typedef struct fields {
	size_t cap;
	size_t len;
	char** fptr;
	size_t *flen;
} fields_t;

int8_t fields_init(fields_t *f)
{
	f->cap  = 0;
	f->len  = 0;
	f->fptr = NULL;
	f->flen = NULL;
	return 0;
};

//TODO porque é que em vez de detectar os campos, simplesmente, convertem-se

int8_t get_fields(fields_t* f, char* buff, ssize_t r)
{
	// Reset structure
	f->len = 0;
	
	size_t c;
	uint8_t is_space = 0;
	uint8_t old_is_space = 1;
	
	for(ssize_t i=0; i<r; i++) {
		is_space = (buff[i] == ' ' || buff[i] == '\t' || buff[i] == '\n');
		if (!is_space && old_is_space) {
		
			//FIXME: falta error handling
			if (f->len == f->cap) {
				//FIXME: esta operação só devia ser feita no fim
				f->cap += 16;
				f->fptr = realloc(f->fptr, f->cap * sizeof(char*));
				f->flen = realloc(f->flen, f->cap * sizeof(size_t));
			}
			
			(f->fptr)[f->len] = buff + i;
			f->len ++;

			old_is_space = 0;
			c = 0;
		
		} else if (is_space && !old_is_space) {
			(f->flen)[f->len - 1] = c;
			old_is_space = 1;
		}

		c++;
	}

	//Check if the field is still being read
	//Note: the capacity for this information has already been alocated
	if (!old_is_space) {
		(f->flen)[f->len - 1] = c;
	}
	
	return 0;
}


int main()
{
	char*   buff = NULL;
	size_t  n    = 0;
	ssize_t r;

	fields_t f;
	fields_init(&f);
	

	while(1){
		r = getline(&buff, &n, stdin);
		if (r==-1)
			break;

		// Obter a posição dos campos
		get_fields(&f, buff, r);

		// Converter cada campo
		// - verificar se é um número (caso não seja, anula-se a coluna)
		// - 
			
		for(size_t i=0; i<f.len; i++) {
			printf("%s\t%lu\n", (f.fptr)[i], (f.flen)[i]);
		}
	}

	
	return 0;
}
