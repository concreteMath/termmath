#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "lib/simple_xoshiro256ss.c"

/*
math-randi v0.3
	- Utilização do xoshiro265starstar para gerar números aleatórios
	- Utilização de variáveis de 64bit
	- Input opcional da quantidade de números aleatórios a gerar
	- Output de um array de números aleatórios, se assim se desejar.

a melhorar:
	- para ser "portable" entre sistemas operativos, poderia gerar uma seed a
	  partir do time, mas teria que ser em nanosegundos.
*/

/*
Este programa serve para escolher um número "pequeno" ao acaso.
Talvez possa meter uma flag para gerar um lote deles.
*/

uint8_t is_uint(char* str){
    uint8_t i = 0;
    char c; 

    while (1) {

        c = str[i];

        if (c == 0) {
            break;
        }

        if ((c<48) || (c>57)) {
            return 0;
        }

        i++;
    }
    // Se chegou aqui, então é um número inteiro >= 0
    return 1;
}


uint64_t str2uint64(char *str){
    if (!is_uint(str)) {
        printf("%s is not a unsigned integer\n", str);
        exit(1);
    }
	// Como já foi verificado que todos os dígitos estão no intervalo [0,9],
	// então a função "strtoul" nunca vai retornar nenhum erro;
	char* dummy;
    return strtoul(str, &dummy, 10);
}


int usage(uint8_t exit_code){
	printf("usage: math-randi NUM_LO NUM_HI [N_NUMBERS]\n");
	exit(exit_code);
	return 0;
}


int main(int argc, char *argv[]){
	
    if ((argc < 3) != (argc >4)) {
		usage(1);
    }
	
	// Quantity of random numbers to be generated. Default is 1.
	uint32_t N = 1;
	if (argc==4) N = (uint32_t)str2uint64(argv[3]);

    uint64_t n0 = str2uint64(argv[1]);
    uint64_t n1 = str2uint64(argv[2]);

    if (n1 < n0) {
        printf("Invalid range: %lu menor que %lu\n", n1, n0);
        exit(1);
    }
	
	// NOTE:
	// Because the range ends are 64bit and the random number generator outputs
	// numbers of all uin64_t range, then there is no need to verify if those
	// ends are within the range.

    uint64_t delta = n1 - n0 + 1;
	
	// If both ends of the range are equal, then it will return one of the ends.
	if (delta == 1) {
		printf("%lu\n", n0);
		return 0;
	}
    
	xoshiro256ss_urandomSeed();

	uint32_t i;
	for (i=0; i<N; i++) {
		printf("%lu\n", xoshiro256ss_uniform(delta) + n0);
	}
    
    return 0;
}
