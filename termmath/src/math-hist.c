//TODO
// Introduzir as opções de binning
// Determinar os pontos fracos e reforçá-los
// Repensar a arquitectura e modularidade.
//	- Separar binning do print ?
// No entanto quero máxima performance
//	- Talvez ler várias linhas do stdin de uma vez só
// Tentar fazer as coisas mais em batch, mas sem descurar a perspectiva
// de stream.

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

// Podia dar o stream do stdin e ir indo testando char a char até chegar a um \n
// Podia fazer uma conversão adaptativa, isto é, conforme os tamanho dos bins
// vão aumentando, vou precisando de cada vez menos precisão e menos dígitos a
// ser convertidos.
// Será que consigo fazer conversões de dígitos em grupos independentes uns dos
// outros?
double fast_str2float(char* buff, size_t n)
{
	double num = 0;
	double sign = 1.0;
	int8_t sep = 0;
	if (n==0)
		return 0;
	
	int64_t mantissa = 0;
	int64_t exponent = 0;
	
	//TODO Falta verificar o whitespace;

	// O primeiro char pode ser +,-,. ou um digito.
	if (buff[0] >= '0' && buff[0] <= '9') {
		mantissa = buff[0] - '0';
	}else if (buff[0] == '-') {
		sign = -1.0;
	}else if (buff[0] == '.') {
		exponent = -1;
		sep = 1;
	}else if (buff[0] != '+') {
		return 0.0;
	}
	
	// FIXME este ciclo está a ser um massacre de tempo
	// O problema não é do cálculo, mas sim de simplesmente do ciclo em si.
	//for (size_t i=1; i<n; i++) {
	for (size_t i=1; i<n; i++) {
		mantissa = mantissa*10 + (buff[i] - '0');
	}
	
	/*
	// MANTISSA
	for(size_t i=1; i<n; i++) {
		if 
	}
	*/
	
	// Talvez possa compor o número com operações bitwise
	num = sign * (double)mantissa;
	
	return num;
}

/*
double stream_str2float()
{
	double num = 0;
	double sign = 1.0;
	int8_t sep = 0;
	
	int64_t mantissa = 0;
	int64_t exponent = 0;
	
	//TODO Falta verificar o whitespace;
	char c = fgetc(stdin);

	// O primeiro char pode ser +,-,. ou um digito.
	if (c >= '0' && c <= '9') {
		mantissa = c - '0';
	}else if (c == '-') {
		sign = -1.0;
	}else if (c == '.') {
		exponent = -1;
		sep = 1;
	}else if (c != '+') {
		return 0.0;
	}
	
	// FIXME este ciclo está a ser um massacre de tempo
	// O problema não é do cálculo, mas sim de simplesmente do ciclo em si.
	//for (size_t i=1; i<n; i++) {
	while(1) {
		c = fgetc(stdin);
		if (c == '\n')
			break;
		mantissa = mantissa*10 + (c - '0');
	}
	
	
	// Talvez possa compor o número com operações bitwise
	num = sign * (double)mantissa;
	
	return num;
}
*/


/*
int8_t tokenize(char* line)
{
	size_t i = 0;
	//while(line[i] != '\0') {
		
}
*/

// FIXME: deverei retornar uma estrutura com os dois vectores ou imprimidir
//        directamente?
// * Eu queria minimizar o uso da memória.
int8_t standard_binning(double* v, int32_t v_len, int32_t n_bins, double v_min,
	double v_max)
{
	int32_t i;

	// Caso os valores sejam todos iguais, retorna-se um "dirac".
	if (v_max == v_min) {
		printf("%g\t%d\n", v_min, v_len);
		for (i=1; i<n_bins; i++) {
			printf("%g\t%lu\n", v_min, 0L);
		}
		/*
		bins[0] = v_len;
		for(int i=0; i<v_len; i++)
			x[i] = v_min;
		*/
		return 0;
	}

	// O bin extra serve para contar os items v_max.
	int64_t* bins = calloc(n_bins+1, sizeof(uint64_t));
	double*  x    = calloc(n_bins, sizeof(double));

	double db = (v_max - v_min) / (double)n_bins;

	// Os marcadores dos bins são centrados.
	for(i=0; i<n_bins; i++) {
		x[i] = v_min + ((double)i + 0.5)*db;
	}

	int32_t b;
	for (i=0; i<v_len; i++) {
		b = (int32_t)((v[i]-v_min)/db);
		bins[b] += 1;
	}
	
	// Como o ultimo bin é fechado superiormente, então adicionam-se os items
	// do bin extra.
	bins[n_bins-1] += bins[n_bins];

	for (i=0; i<n_bins; i++) {
		printf("%g\t%ld\n", x[i], bins[i]);
	}

	return 0;
}


int main() {
	//int32_t N = 20;
	//int64_t* bins = malloc(N*sizeof(int64_t));
	size_t vec_len       = 0;
	size_t vec_cap       = 512;
	size_t vec_cap_delta = 512;
	double* vec = malloc(vec_cap*sizeof(double)); 

	size_t n    = 0;
	ssize_t ret = -1;

	char* buffer = NULL;
	char* buff_end = NULL;

	double num  = 0;
	
	//FIXME
	double num_min = strtod("inf",NULL);
	double num_max = strtod("-inf",NULL);

	while(!feof(stdin)) {
		ret = getline(&buffer, &n, stdin);
		if (ret==-1)
			break;
		
		// FIXME Este conversor está a comer o tempo todo
		//num = strtod(buffer, &buff_end);

		//num = 1.0;
		//num = (double)buffer[0];
		num = fast_str2float(buffer, ret);

		//num = stream_str2float();

		/*
		if (buffer == buff_end) {
			printf("no read\n");
			continue;
		}
		*/


		if(0) {
			printf("read: %f\n", num);
			printf("read: %s", buffer);
			printf("ret: %ld\n", ret);
			printf("n: %lu\n", n);
			printf("\n");
		}
		
		// GET LIMITS
		if (num < num_min)
			num_min = num;
		if (num > num_max)
			num_max = num;
		

		if (vec_len == vec_cap) {
			vec_cap += vec_cap_delta;
			vec = realloc(vec, vec_cap*sizeof(double));
		}

		vec[vec_len] = num;
		vec_len++;
	}
	//printf("min:%g   max:%g\n", num_min, num_max);

	/*
	for (size_t i=0; i<vec_len; i++) {
		printf("%lu %g\n", i, vec[i]);
	}
	*/
	
	standard_binning(vec, vec_len, 20, num_min, num_max);

	return 0;
}
