# pragma once

#include "libtermmath.h"

/*
	buff_t initialization to default values

	To be safe, it assumes no previous allocations.
	The allocations in this function must be ready for realloc
*/
int Buff_init(buff_t* b)
{
	b->rows_len = 0;
	b->cols_len = 0;
	b->rows = (double**)malloc(sizeof(double*) * 0);
	b->cols_active = (int*)malloc(sizeof(int) * 0);
	b->row_index = -1;
	return 0;
}


/*
	Reads stdin and populates a numerical matrix.
*/
buff Buff_read_stdin() {
	buff_t b;

