#!/usr/bin/python3

# Cumsum
# (2019-08-07)
# Soma cumulativa

# FIXME: reutilizar as listas de leitura e acumulação

# TODO: Deverei replicar o comportament do awk, no que respeita a somar strings
# ou campos vazios?

from sys import stdin, stdout

cumsum_len = 0
values = []
types  = []
while True:
	# INPUT
	line = stdin.readline()
	if line == "":
		break
	
	# Estou a assumir que o python vai separar os vários campos por conjuntos de
	# espaços e tabs.
	input_fields = line.strip().split()
	input_len = len(input_fields)
	

	# AJUSTE DOS VECTORES
	if input_len > cumsum_len:
		values += [0]*(input_len - cumsum_len)
		types  += [0]*(input_len - cumsum_len)
		cumsum_len = input_len

	if cumsum_len == 0:
		continue
	

	# CONVERSÕES
	# Teste dos campos de input
	# Codigos dos types: -1:empty 0:int, 1:float, 2:NaN
	for i in range(input_len):

		# Isto basicamente testa se o field é um número
		#TODO trocar por um if
		try:
			x = float(input_fields[i])
		except ValueError:
			types[i] = 2
			continue

		# Aqui afina a classificação do número, entre inteiro ou float
		#TODO trocar por um if
		try:
			x_int = int(input_fields[i])
			values[i] += x_int
			continue
		except ValueError:
			# É float
			type[i] = 1
			values[i] += x
	
	# OUTPUT
	stdout.write("%g"%values[0])
	for i in range(1, cumsum_len):
		if types[i] == 2:
			stdout.write('\t')
			continue
		
		# Print com formatação inteira, caso ambos sejam inteiros.
		if types[i]==0:
			stdout.write("\t%d"%values[i])
		else:
			stdout.write("\t%g"%values[i])
	
	stdout.write("\n")

exit(0)
