#!/bin/bash

# Este script serve para escolher um número "pequeno" ao acaso.
# Uma versão mais robusta terá que ser feita em c, dado que para tirar
# muitos números ao calhas, este programa é muito lento.

MAX_RANDOM=32768

n0=$1
n1=$2

if [ $# -ne 2 ]; then
    echo É apenas necessário o numero inicial e final.
    exit 1
fi

if [ $n1 -lt $n0 ]; then
    echo $n1 menor que $n0
    exit 1
fi

if [ $n0 -gt $MAX_RANDOM ] || [ $n1 -gt $MAX_RANDOM ]; then
    echo Os numeros excedem o tamanho maximo $MAX_RANDOM
    exit 1
fi

delta=$((n1 - n0 + 1))
max_tmp=$((MAX_RANDOM - MAX_RANDOM%delta))

while true; do
    r=$RANDOM
    if [ $r -gt $max_tmp ]; then
        continue
    else
        break
    fi
done

r=$((r%delta + n0))
echo $r
