#!/usr/bin/python3

# math-binning v2.1 (2018-10-09)
# - Instrodução do binning logarítmico

from sys import stdin, stdout, argv
from math import log10


def usage():
	print("usage: %s [-n n_bins] [-i] [-l]"%argv[0])
	print("-n number of bins")
	print("-i integer binning")
	print("-l logaritmic binning")
	exit(2)


def error_msg(msg):
	print("[math-binning] " + msg)


def parse_input_param():

	# Verificação de inputs
	if len(argv) > 5:
		error_msg("Excess input parameters.")
		usage()

	# Configurações por defeito
	# Numero de bins: 20
	# Binning de inteiros: False
	# Binning logaritmico: False
	config = {"n_bins": 20, "int_bin": False, "log_bin": False}

	par_used = {"-n": False, "-i": False, "-l": False}
	
	i = 1
	while i < len(argv):
		arg = argv[i]

		if arg not in par_used:
			error_msg("Parameter \"%s\" not valid."%arg)
			usage()
			
		if par_used[arg]:
			error_msg("Parameter %s already used."%arg)
			usage()
		par_used[arg] = True

		if arg == "-n":
			# Caso não haja parâmetro de input
			if i+1 == len(argv):
				error_msg("No number of bins specified.")
				usage()
			
			n_bins_str = argv[i+1]

			# Caso o próximo input não seja um número inteiro
			if not n_bins_str.isdigit():
				error_msg("Invalid number of bins.")
				usage()

			n_bins = int(n_bins_str)

			# Não se pode fazer binning com 0 bins.
			if n_bins == 0:
				error_msg("Number of bins must be non zero.")
				usage()

			config["n_bins"] = int(n_bins_str)
			# Avanço por causa do número
			i+=1
		elif arg == "-i":
			config["int_bin"] = True
		elif arg == "-l":
			config["log_bin"] = True
			
		i += 1

	return config


def read_stdin_stream():
	data = []
	while True:
		line = stdin.readline()
		if line == "":
			break
		data += [float(line)]

	if len(data) == 0:
		error_msg("Input data empty.")
		exit(1)
	
	return data


# Vou usar uma espécie de binning inteiro, em que o indice 'b' do bin de um
# valor 'y', para um bin de tamanho 'db' é dado por b = floor((y-y_min)/dv), mas
# o marcador do bin é centrado.
# Todos os bins correspondem a intervalos fechados em baixo e abertos em cima,
# excepto o último intervalo que é fechado em cima.
# Este binning foi baseado no 'octave'.
def standard_binning(v, n_bins):
	n_vals = len(v)
	v_min  = min(v)
	v_max  = max(v)
	
	# Caso os valores sejam todos iguais, retorna-se um "dirac".
	if v_max == v_min:
		bins = [n_vals] + [0]*(n_bins-1)
		x    = [v_min]*n_bins
		return(x,bins)

	db = (v_max - v_min) / n_bins

	# Os marcadores dos bins são centrados.
	x    = [v_min + (i+.5)*db for i in range(n_bins)]
	# O bin extra serve para contar os items v_max.
	bins = [0]*(n_bins+1)

	for i in range(n_vals):
		b = int((v[i]-v_min)//db)
		bins[b] += 1
	
	# Como o ultimo bin é fechado superiormente, então adicionam-se os items
	# do bin extra.
	bins[-2] += bins[-1]
	return (x, bins[:-1])


def integer_binning(v):
	v_min = int(min(v))
	v_max = int(max(v))
	x    = [i for i in range(v_min, v_max + 1)]
	bins = [0]*(v_max - v_min + 1)

	for n in v:
		b = int(n) - v_min
		bins[b] += 1
	
	return (x, bins)


# Depende de math.log10 e standard_binning()
def logaritmic_binning(v, n_bins):
	# Filtragem e conversão para espaço logarítmico
	v1 = [ log10(a) for a in v if a>0]
	x, bins = standard_binning(v1, n_bins)
	# Conversão do suporte para espaço linear
	x1 = [ 10.0**a for a in x]
	return x1, bins


def print_bins(x, bins, int_bin):
	n_bins = len(x)
	str_out = ""

	if int_bin:
		for k in range(n_bins):
			str_out += "%d\t%d\n"%(x[k], bins[k])
	else:
		for k in range(n_bins):
			str_out += "%g\t%d\n"%(x[k], bins[k])

	stdout.write(str_out)
	


################################################################################
################################################################################


# Parsing do parâmetros de input
config  = parse_input_param()
n_bins  = config["n_bins"]
int_bin = config["int_bin"]
log_bin = config["log_bin"]

# Error handling
if int_bin and log_bin:
	error_msg("logaritmic and integer binning cannot be used simultaneously")
	exit(2)

# Leitura do stream
data = read_stdin_stream()

# Binning
if int_bin:
	x, bins = integer_binning(data)
elif log_bin:
	x, bins = logaritmic_binning(data, n_bins)
else:
	x, bins = standard_binning(data, n_bins)

# Apresentação do resultado
print_bins(x, bins, int_bin)
