#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

// math-randi v0.1

/*
Este programa serve para escolher um número "pequeno" ao acaso.
Talvez possa meter uma flag para gerar um lote deles.
*/


int rand_int(n){
    // Returns a integer random value between [0,n[
    int lim_max=RAND_MAX-RAND_MAX%n;
    int r;
    do{
        r = rand();
    }while(r>=lim_max);
    return r%n;
}




uint8_t is_uint(char* str){
    uint8_t i = 0;
    char c; 

    while (1) {

        c = str[i];

        if (c == 0) {
            break;
        }

        if ((c<48) || (c>57)) {
            return 0;
        }

        i++;
    }
    // Se chegou aqui, então é um número inteiro >= 0
    return 1;
}




uint32_t str2uint(char *str){
    if (!is_uint(str)) {
        printf("%s não é um número inteiro >= 0\n", str);
        exit(1);
    }
    return atoi(str);
}



int main(int argc, char *argv[]){

    if (argc != 3) {
        printf("É necessário o numero inicial e final.\n");
        exit(1);
    }

    int n0 = str2uint(argv[1]);
    int n1 = str2uint(argv[2]);

    if (n1 < n0) {
        printf("Intervalo inválido: %d menor que %d\n", n1, n0);
        exit(1);
    }

    if ((n0 >= RAND_MAX) || ( n1 >= RAND_MAX )) {
        printf("Os numeros excedem o tamanho maximo %i\n", RAND_MAX);
        exit(1);
    }

    int delta = n1 - n0 + 1;
    
    srand(time(NULL));

    printf("%i\n", rand_int(delta) + n0);
    
    return 0;
}
