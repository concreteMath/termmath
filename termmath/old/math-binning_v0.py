#!/usr/bin/python3

from sys import stdin, stdout, argv
import numpy as np

if len(argv) > 2:
	print("Not cool.")
	exit(1)

# Determinação do número de bins
n_bins = 100
if len(argv) == 2:
	n_bins = int(argv[1])

# Leitura do stream
str_data = stdin.read()
data = np.fromstring(str_data, sep = '\n')

h = np.histogram(data, n_bins)

str_out = ""
for k in range(len(h[0])):
	str_out += "%g\t%g\n"%(h[1][k], h[0][k])

stdout.write(str_out)
