#!/usr/bin/python3

# math-binning v1
# Nesta versão, vou-me livrar da biblioteca do numpy
# Quero também ter uma opção para binning discreto

from sys import stdin, stdout, argv

if len(argv) > 2:
	print("Not cool.")
	exit(1)

# Determinação do número de bins
n_bins = 20
if len(argv) == 2:
	n_bins = int(argv[1])

# Preparação para armazenar dados
data = []

# Leitura do stream
while True:
	line = stdin.readline()
	if line == "":
		break
	data += [float(line)]

if len(data) == 0:
	exit(1)


# Binning
bins = [0]*n_bins
v_min = min(data)
v_max = max(data)
dv = (v_max - v_min)/(n_bins-1)

for n in data:
	b = int((n-v_min)//dv)
	bins[b] += 1


# Apresentação do resultado
str_out = ""
for k in range(n_bins):
	str_out += "%g\t%g\n"%(v_min + k*dv, bins[k])

stdout.write(str_out)
