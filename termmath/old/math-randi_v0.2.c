#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

/*
math-randi v0.2
	- Melhoria da mensagem do usage
	- Criação de uma nova função para gerar seeds a partir do urandom

a melhorar:
	- poderia gerar uma seed a partir do time, mas teria que ser em
	  nanosegundos.
	- poderia ir além dos 32bit do rand, talvez usando 2 rands. Quase que dá
	  vontade de gerar os números directamente do urandom e tá feito.

*/

/*
Este programa serve para escolher um número "pequeno" ao acaso.
Talvez possa meter uma flag para gerar um lote deles.
*/


// Returns a integer random value between [0,n[
int rand_int(int n){
    int lim_max=RAND_MAX-RAND_MAX%n;
    int r;
    do{
        r = rand();
    }while(r>=lim_max);
    return r%n;
}


// Gera uma seed a partir do ficheiro /dev/urandom típico de um sistema unix
// Verifiquei no código fonte do random do python que também faz seed lendo o
// /dev/urandom
int get_seed_urandom(){
	FILE* fp = fopen("/dev/urandom", "rb");
	int c0 = fgetc(fp);
	int c1 = fgetc(fp);
	fclose(fp);
	int r = (c1<<8) | c0;
	return r;
}


uint8_t is_uint(char* str){
    uint8_t i = 0;
    char c; 

    while (1) {

        c = str[i];

        if (c == 0) {
            break;
        }

        if ((c<48) || (c>57)) {
            return 0;
        }

        i++;
    }
    // Se chegou aqui, então é um número inteiro >= 0
    return 1;
}


uint32_t str2uint(char *str){
    if (!is_uint(str)) {
        printf("%s não é um número inteiro >= 0\n", str);
        exit(1);
    }
    return atoi(str);
}


int main(int argc, char *argv[]){

    if (argc != 3) {
        printf("usage: %s NUM_LO NUM_HI\n", argv[0]);
        exit(1);
    }

    int n0 = str2uint(argv[1]);
    int n1 = str2uint(argv[2]);

    if (n1 < n0) {
        printf("Intervalo inválido: %d menor que %d\n", n1, n0);
        exit(1);
    }

    if ((n0 >= RAND_MAX) || ( n1 >= RAND_MAX )) {
        printf("Os numeros excedem o tamanho maximo %i\n", RAND_MAX);
        exit(1);
    }

    int delta = n1 - n0 + 1;
    
    int seed = get_seed_urandom();
    srand(seed);

    printf("%i\n", rand_int(delta) + n0);
    
    return 0;
}
