#!/usr/bin/python3

# math-stats
# Devolve estatísticas sobre os dados no input
# Vou considerar todas as colunas. Caso uma linha tenha conteúdo não numérico,
# as estatísticas devolvidas são NaN.

import sys

# FIXME: Arranjar uma forma mais sistemática de lidar com inputs
fPrintLabels = True

if len(sys.argv) > 1:
	if sys.argv[1] == "-l":
		fPrintLabels = False
	else:
		print("[math-stats] error: parameter", sys.argv[1], "not valid",
		      file = sys.stderr)


sum0  = []
sum1  = []
sum2  = []
valid = []
N = 0
while True:
	line = sys.stdin.readline()
	if line == "":
		break
	
	# Estou a assumir que o python vai separar os vários campos por conjuntos de
	# espaços e tabs.
	fields = line.split()
	nf = len(fields)

	lim_max = min(N, nf)
	
	# Somas para os campos existentes
	for i in range(lim_max):

		if not valid[i]:
			continue
		
		# Isto basicamente testa se o field é um número
		try:
			v = float(fields[i])
		except ValueError:
			valid[i] = False
			continue
		
		sum0[i] += 1
		sum1[i] += v
		sum2[i] += v**2
	
	# Somas novas
	n_rest = nf - lim_max
	sum0_new  = [0]    * n_rest
	sum1_new  = [0.0]  * n_rest
	sum2_new  = [0.0]  * n_rest
	valid_new = [True] * n_rest
	for i in range(n_rest):

		try:
			v = float(fields[lim_max + i])
		except ValueError:
			valid_new[i] = False
			continue

		sum0_new[i] = 1
		sum1_new[i] = v
		sum2_new[i] = v**2
	
	sum0  += sum0_new
	sum1  += sum1_new
	sum2  += sum2_new
	valid += valid_new

	if nf > N:
		N = nf

if N == 0:
	exit(2)


#### PRINT ####

nan_num = float('nan')

# Output de todos os campos excepto o último
if fPrintLabels:
	sys.stdout.write("%19s%13s%13s\n"%("mean","std","var"))

for i in range(N):
	
	mean = nan_num
	var  = nan_num
	std  = nan_num

	if valid[i]:
		mean = sum1[i]/sum0[i]
	
		if sum0[i] > 1:
			# Estou a usar a "unbiased sample variance"
			var = (sum2[i] - sum0[i]*mean**2)/(sum0[i] - 1.0)
			std = var**(0.5)

	if fPrintLabels:
		sys.stdout.write("col %-2i"%(i+1))
	sys.stdout.write("%13.5g%13.5g%13.5g\n"%(mean, std, var))

exit(0)
