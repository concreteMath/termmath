# termmath

Mathematics utilities for the unix terminal

- math-cumsum
- math-total
- math-diff
- math-randi
- math-hist
- math-stats
