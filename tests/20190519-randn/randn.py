#!/usr/bin/python3

import random
import sys

if len(sys.argv) != 2:
	exit(2)

n = int(sys.argv[1])

for i in range(n):
	print(random.gauss(0.0,1.0))
