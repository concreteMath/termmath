#!/bin/bash

n_list=(10 20 -107849074923757593759375939 1e300 1e400 1e-400 banana xxx14 12xx34 NAN INF)
for n in ${n_list[@]}; do
	echo $n
	./float $n
	echo
done
