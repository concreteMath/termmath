#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

// Podia dar o stream do stdin e ir indo testando char a char até chegar a um \n
// Podia fazer uma conversão adaptativa, isto é, conforme os tamanho dos bins
// vão aumentando, vou precisando de cada vez menos precisão e menos dígitos a
// ser convertidos.
// Será que consigo fazer conversões de dígitos em grupos independentes uns dos
// outros?
double fast_str2float(char* buff, size_t n)
{
	double num = 0;
	double sign = 1.0;
	int8_t sep = 0;
	if (n==0)
		return 0;
	
	int64_t mantissa = 0;
	int64_t exponent = 0;
	
	//TODO Falta verificar o whitespace;

	// O primeiro char pode ser +,-,. ou um digito.
	if (buff[0] >= '0' && buff[0] <= '9') {
		mantissa = buff[0] - '0';
	}else if (buff[0] == '-') {
		sign = -1.0;
	}else if (buff[0] == '.') {
		exponent = -1;
		sep = 1;
	}else if (buff[0] != '+') {
		return 0.0;
	}
	
	// FIXME este ciclo está a ser um massacre de tempo
	// O problema não é do cálculo, mas sim de simplesmente do ciclo em si.
	//for (size_t i=1; i<n; i++) {
	for (size_t i=1; i<n; i++) {
		mantissa = mantissa*10 + (buff[i] - '0');
	}
	
	/*
	// MANTISSA
	for(size_t i=1; i<n; i++) {
		if 
	}
	*/
	
	// Talvez possa compor o número com operações bitwise
	num = sign * (double)mantissa;
	
	return num;
}

/*
double stream_str2float()
{
	double num = 0;
	double sign = 1.0;
	int8_t sep = 0;
	
	int64_t mantissa = 0;
	int64_t exponent = 0;
	
	//TODO Falta verificar o whitespace;
	char c = fgetc(stdin);

	// O primeiro char pode ser +,-,. ou um digito.
	if (c >= '0' && c <= '9') {
		mantissa = c - '0';
	}else if (c == '-') {
		sign = -1.0;
	}else if (c == '.') {
		exponent = -1;
		sep = 1;
	}else if (c != '+') {
		return 0.0;
	}
	
	// FIXME este ciclo está a ser um massacre de tempo
	// O problema não é do cálculo, mas sim de simplesmente do ciclo em si.
	//for (size_t i=1; i<n; i++) {
	while(1) {
		c = fgetc(stdin);
		if (c == '\n')
			break;
		mantissa = mantissa*10 + (c - '0');
	}
	
	
	// Talvez possa compor o número com operações bitwise
	num = sign * (double)mantissa;
	
	return num;
}
*/


int main() {
	char* buffer = NULL;
	char* buff_end = NULL;
	double num  = 0;

	while(!feof(stdin)) {
		ret = getline(&buffer, &n, stdin);
		if (ret==-1)
			break;
		
		// FIXME Este conversor está a comer o tempo todo
		//num = strtod(buffer, &buff_end);

		//num = 1.0;
		//num = (double)buffer[0];
		num = fast_str2float(buffer, ret);

		//num = stream_str2float();

		/*
		if (buffer == buff_end) {
			printf("no read\n");
			continue;
		}
		*/

	}
	return 0;
}
