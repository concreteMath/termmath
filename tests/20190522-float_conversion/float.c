#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char* argv[])
{	
	char* str0 = malloc(strlen(argv[1]) + 1);
	char* str1 = NULL;;
	strcpy(str0, argv[1]);
	errno = -666;
	double x = strtod(str0, &str1);
	printf("errno: %i\n", errno);
	printf("    x: %g\n", x);
	printf(" str0: %p\n", str0);
	printf(" str1: %p\n", str1);
	printf("delta: %ld\n", str1 - str0);
	return 0;
}
